#!/usr/bin/env python3

# This file is part of the replication artifact for
# difference verification with conditions:
# https://gitlab.com/sosy-lab/research/data/difference-data
#
# SPDX-FileCopyrightText: 2020 Dirk Beyer <https://sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

import argparse
from pathlib import Path
import shutil


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--target-dir", default="precisions")
    parser.add_argument("--target-names", default="predmap.txt")
    parser.add_argument("resultfiles_dir", nargs="+")

    args = parser.parse_args()

    count = 0
    for d in args.resultfiles_dir:
        base_path = Path(d).absolute()

        predmaps = [p.relative_to(base_path) for p in base_path.rglob('predmap.txt')]
        for p in predmaps:
            task_name = str(p).split('/')[0]
            assert task_name.endswith('.yml'), f"{str(p)}: {task_name}"
            source = base_path / p

            target_dir = Path(args.target_dir) / task_name
            target_dir.mkdir(parents=True)
            target = target_dir / args.target_names
            shutil.move(source, target)
            count += 1

print(f"Moved {count} files")
