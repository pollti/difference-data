(set-info :source |printed by MathSAT|)
(declare-fun a20 () (_ BitVec 32))
(declare-fun a25 () (_ BitVec 32))
(declare-fun a12 () (_ BitVec 32))
(declare-fun a15 () (_ BitVec 32))
(declare-fun a16 () (_ BitVec 32))
(declare-fun a2 () (_ BitVec 32))
(declare-fun a27 () (_ BitVec 32))
(declare-fun a9 () (_ BitVec 32))
(declare-fun m_pc () (_ BitVec 32))
(declare-fun t1_pc () (_ BitVec 32))
(declare-fun m_st () (_ BitVec 32))
(declare-fun E_M () (_ BitVec 32))
(declare-fun t1_st () (_ BitVec 32))
(declare-fun local () (_ BitVec 32))
(declare-fun token () (_ BitVec 32))
(declare-fun E_1 () (_ BitVec 32))

*:
(assert (let ((.def_107113 (= a20 (_ bv5 32)))).def_107113))

calculate_output8 N29943:
(assert false)
(assert (let ((.def_107113 (= a20 (_ bv5 32)))).def_107113))

main1 N30136:
(assert (let ((.def_107113 (= a20 (_ bv5 32)))).def_107113))
(assert (let ((.def_154455 (= a25 (_ bv1 32)))).def_154455))
(assert (let ((.def_154458 (= a12 (_ bv4 32)))).def_154458))
(assert (let ((.def_154461 (= a15 (_ bv1 32)))).def_154461))
(assert (let ((.def_154464 (= a16 (_ bv1 32)))).def_154464))
(assert (let ((.def_154467 (= a2 (_ bv10 32)))).def_154467))
(assert (let ((.def_154470 (= a27 (_ bv1 32)))).def_154470))
(assert (let ((.def_154473 (= a9 (_ bv18 32)))).def_154473))
(assert (let ((.def_585537 (= a2 (_ bv13 32)))).def_585537))
(assert (let ((.def_585539 (= a2 (_ bv9 32)))).def_585539))
(assert (let ((.def_585541 (= a2 (_ bv12 32)))).def_585541))
(assert (let ((.def_585543 (= a20 (_ bv6 32)))).def_585543))
(assert (let ((.def_585545 (= a20 (_ bv7 32)))).def_585545))
(assert (let ((.def_585547 (= a12 (_ bv3 32)))).def_585547))
(assert (let ((.def_585549 (= a12 (_ bv2 32)))).def_585549))
(assert (let ((.def_585551 (= a12 (_ bv5 32)))).def_585551))
(assert (let ((.def_585553 (= a2 (_ bv11 32)))).def_585553))
(assert (let ((.def_585555 (= a9 (_ bv16 32)))).def_585555))
(assert (let ((.def_585557 (= a12 (_ bv6 32)))).def_585557))
(assert (let ((.def_585559 (= a9 (_ bv17 32)))).def_585559))

error N30154:
(assert false)
(assert (let ((.def_107113 (= a20 (_ bv5 32)))).def_107113))

eval N30288:
(assert (let ((.def_921871 (= m_pc (_ bv0 32)))).def_921871))
(assert (let ((.def_107113 (= a20 (_ bv5 32)))).def_107113))
(assert (let ((.def_922424 (= t1_pc (_ bv0 32)))).def_922424))
(assert (let ((.def_922426 (= m_pc (_ bv1 32)))).def_922426))
(assert (let ((.def_922428 (= t1_pc (_ bv1 32)))).def_922428))
(assert (let ((.def_922431 (= m_st (_ bv0 32)))).def_922431))
(assert (let ((.def_923144 (= E_M (_ bv1 32)))).def_923144))
(assert (let ((.def_923147 (= t1_st (_ bv0 32)))).def_923147))
(assert (let ((.def_923150 (bvadd (_ bv1 32) local)))(let ((.def_923152 (= .def_923150 token))).def_923152)))
(assert (let ((.def_924151 (= local token))).def_924151))
(assert (let ((.def_924154 (= E_1 (_ bv1 32)))).def_924154))
(assert (let ((.def_925452 (= E_M (_ bv0 32)))).def_925452))
(assert (let ((.def_925454 (= E_1 (_ bv0 32)))).def_925454))

start_simulation N30438:
(assert (let ((.def_921871 (= m_pc (_ bv0 32)))).def_921871))
(assert (let ((.def_107113 (= a20 (_ bv5 32)))).def_107113))
(assert (let ((.def_922424 (= t1_pc (_ bv0 32)))).def_922424))
(assert (let ((.def_922426 (= m_pc (_ bv1 32)))).def_922426))
(assert (let ((.def_922428 (= t1_pc (_ bv1 32)))).def_922428))
(assert (let ((.def_923144 (= E_M (_ bv1 32)))).def_923144))
(assert (let ((.def_923147 (= t1_st (_ bv0 32)))).def_923147))
(assert (let ((.def_922431 (= m_st (_ bv0 32)))).def_922431))
(assert (let ((.def_925452 (= E_M (_ bv0 32)))).def_925452))

