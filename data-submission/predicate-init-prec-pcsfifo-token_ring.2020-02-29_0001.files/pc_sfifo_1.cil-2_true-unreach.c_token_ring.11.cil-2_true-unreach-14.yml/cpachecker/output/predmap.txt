(set-info :source |printed by MathSAT|)
(declare-fun c_last_read () (_ BitVec 32))
(declare-fun p_last_write () (_ BitVec 32))
(declare-fun c_num_read () (_ BitVec 32))
(declare-fun p_num_write () (_ BitVec 32))
(declare-fun c_dr_pc () (_ BitVec 32))
(declare-fun q_free () (_ BitVec 32))
(declare-fun q_buf_0 () (_ BitVec 32))
(declare-fun p_dw_pc () (_ BitVec 32))
(declare-fun q_read_ev () (_ BitVec 32))
(declare-fun c_dr_st () (_ BitVec 32))
(declare-fun p_dw_st () (_ BitVec 32))
(declare-fun m_pc () (_ BitVec 32))
(declare-fun t1_pc () (_ BitVec 32))
(declare-fun t2_pc () (_ BitVec 32))
(declare-fun t3_pc () (_ BitVec 32))
(declare-fun t4_pc () (_ BitVec 32))
(declare-fun t5_pc () (_ BitVec 32))
(declare-fun t6_pc () (_ BitVec 32))
(declare-fun t7_pc () (_ BitVec 32))
(declare-fun t8_pc () (_ BitVec 32))
(declare-fun t9_pc () (_ BitVec 32))
(declare-fun t10_pc () (_ BitVec 32))
(declare-fun t11_pc () (_ BitVec 32))
(declare-fun m_st () (_ BitVec 32))
(declare-fun E_9 () (_ BitVec 32))
(declare-fun E_8 () (_ BitVec 32))
(declare-fun E_7 () (_ BitVec 32))
(declare-fun E_6 () (_ BitVec 32))
(declare-fun E_5 () (_ BitVec 32))
(declare-fun E_4 () (_ BitVec 32))
(declare-fun E_3 () (_ BitVec 32))
(declare-fun E_2 () (_ BitVec 32))
(declare-fun E_M () (_ BitVec 32))
(declare-fun E_10 () (_ BitVec 32))
(declare-fun E_11 () (_ BitVec 32))
(declare-fun t8_st () (_ BitVec 32))
(declare-fun t1_st () (_ BitVec 32))
(declare-fun t5_st () (_ BitVec 32))
(declare-fun t6_st () (_ BitVec 32))
(declare-fun t4_st () (_ BitVec 32))
(declare-fun t3_st () (_ BitVec 32))
(declare-fun t9_st () (_ BitVec 32))
(declare-fun t2_st () (_ BitVec 32))
(declare-fun t7_st () (_ BitVec 32))
(declare-fun t10_st () (_ BitVec 32))
(declare-fun t11_st () (_ BitVec 32))
(declare-fun E_1 () (_ BitVec 32))
(declare-fun local () (_ BitVec 32))
(declare-fun token () (_ BitVec 32))

*:
(assert (let ((.def_660 (= c_last_read p_last_write))).def_660))
(assert (let ((.def_669 (= c_num_read p_num_write))).def_669))

error1 N4:
(assert false)
(assert (let ((.def_660 (= c_last_read p_last_write))).def_660))
(assert (let ((.def_669 (= c_num_read p_num_write))).def_669))

do_write_p N64:
(assert (let ((.def_1004 (= c_dr_pc (_ bv0 32)))).def_1004))
(assert (let ((.def_660 (= c_last_read p_last_write))).def_660))
(assert (let ((.def_669 (= c_num_read p_num_write))).def_669))
(assert (let ((.def_1107 (= q_free (_ bv1 32)))).def_1107))
(assert false)
(assert (let ((.def_1450 (= p_last_write q_buf_0))).def_1450))
(assert (let ((.def_1110 (= p_dw_pc (_ bv1 32)))).def_1110))
(assert (let ((.def_1639 (bvadd (_ bv1 32) c_num_read)))(let ((.def_1640 (= p_num_write .def_1639))).def_1640)))
(assert (let ((.def_2531 (= q_free (_ bv0 32)))).def_2531))
(assert (let ((.def_1642 (= c_dr_pc (_ bv1 32)))).def_1642))
(assert (let ((.def_5332 (= q_read_ev (_ bv1 32)))).def_5332))

do_read_c N100:
(assert (let ((.def_1110 (= p_dw_pc (_ bv1 32)))).def_1110))
(assert (let ((.def_1107 (= q_free (_ bv1 32)))).def_1107))
(assert (let ((.def_660 (= c_last_read p_last_write))).def_660))
(assert (let ((.def_669 (= c_num_read p_num_write))).def_669))
(assert (let ((.def_1450 (= p_last_write q_buf_0))).def_1450))
(assert (let ((.def_1639 (bvadd (_ bv1 32) c_num_read)))(let ((.def_1640 (= p_num_write .def_1639))).def_1640)))
(assert (let ((.def_1642 (= c_dr_pc (_ bv1 32)))).def_1642))
(assert false)

eval1 N170:
(assert (let ((.def_1004 (= c_dr_pc (_ bv0 32)))).def_1004))
(assert (let ((.def_660 (= c_last_read p_last_write))).def_660))
(assert (let ((.def_669 (= c_num_read p_num_write))).def_669))
(assert (let ((.def_1107 (= q_free (_ bv1 32)))).def_1107))
(assert (let ((.def_1380 (= p_dw_pc (_ bv0 32)))).def_1380))
(assert (let ((.def_1642 (= c_dr_pc (_ bv1 32)))).def_1642))
(assert (let ((.def_3954 (= c_dr_st (_ bv0 32)))).def_3954))
(assert (let ((.def_1110 (= p_dw_pc (_ bv1 32)))).def_1110))
(assert (let ((.def_5332 (= q_read_ev (_ bv1 32)))).def_5332))
(assert (let ((.def_5335 (= p_dw_st (_ bv0 32)))).def_5335))
(assert (let ((.def_1450 (= p_last_write q_buf_0))).def_1450))
(assert (let ((.def_1639 (bvadd (_ bv1 32) c_num_read)))(let ((.def_1640 (= p_num_write .def_1639))).def_1640)))

start_simulation1 N221:
(assert (let ((.def_1004 (= c_dr_pc (_ bv0 32)))).def_1004))
(assert (let ((.def_660 (= c_last_read p_last_write))).def_660))
(assert (let ((.def_669 (= c_num_read p_num_write))).def_669))
(assert (let ((.def_1107 (= q_free (_ bv1 32)))).def_1107))
(assert (let ((.def_1380 (= p_dw_pc (_ bv0 32)))).def_1380))
(assert (let ((.def_5332 (= q_read_ev (_ bv1 32)))).def_5332))

error2 N260:
(assert false)
(assert (let ((.def_660 (= c_last_read p_last_write))).def_660))
(assert (let ((.def_669 (= c_num_read p_num_write))).def_669))

eval2 N934:
(assert (let ((.def_8237 (= m_pc (_ bv0 32)))).def_8237))
(assert (let ((.def_660 (= c_last_read p_last_write))).def_660))
(assert (let ((.def_669 (= c_num_read p_num_write))).def_669))
(assert (let ((.def_19060 (= t1_pc (_ bv0 32)))).def_19060))
(assert (let ((.def_19063 (= t2_pc (_ bv0 32)))).def_19063))
(assert (let ((.def_19066 (= t3_pc (_ bv0 32)))).def_19066))
(assert (let ((.def_19069 (= t4_pc (_ bv0 32)))).def_19069))
(assert (let ((.def_19072 (= t5_pc (_ bv0 32)))).def_19072))
(assert (let ((.def_19075 (= t6_pc (_ bv0 32)))).def_19075))
(assert (let ((.def_19078 (= t7_pc (_ bv0 32)))).def_19078))
(assert (let ((.def_19081 (= t8_pc (_ bv0 32)))).def_19081))
(assert (let ((.def_19084 (= t9_pc (_ bv0 32)))).def_19084))
(assert (let ((.def_19087 (= t10_pc (_ bv0 32)))).def_19087))
(assert (let ((.def_19090 (= t11_pc (_ bv0 32)))).def_19090))
(assert (let ((.def_19092 (= m_pc (_ bv1 32)))).def_19092))
(assert (let ((.def_19094 (= t1_pc (_ bv1 32)))).def_19094))
(assert (let ((.def_19096 (= t2_pc (_ bv1 32)))).def_19096))
(assert (let ((.def_19098 (= t3_pc (_ bv1 32)))).def_19098))
(assert (let ((.def_19100 (= t4_pc (_ bv1 32)))).def_19100))
(assert (let ((.def_19102 (= t5_pc (_ bv1 32)))).def_19102))
(assert (let ((.def_19104 (= t6_pc (_ bv1 32)))).def_19104))
(assert (let ((.def_19106 (= t7_pc (_ bv1 32)))).def_19106))
(assert (let ((.def_19108 (= t8_pc (_ bv1 32)))).def_19108))
(assert (let ((.def_19110 (= t9_pc (_ bv1 32)))).def_19110))
(assert (let ((.def_19112 (= t10_pc (_ bv1 32)))).def_19112))
(assert (let ((.def_19114 (= t11_pc (_ bv1 32)))).def_19114))
(assert (let ((.def_19117 (= m_st (_ bv0 32)))).def_19117))
(assert (let ((.def_114091 (= E_9 (_ bv1 32)))).def_114091))
(assert (let ((.def_114094 (= E_8 (_ bv1 32)))).def_114094))
(assert (let ((.def_114097 (= E_7 (_ bv1 32)))).def_114097))
(assert (let ((.def_114100 (= E_6 (_ bv1 32)))).def_114100))
(assert (let ((.def_114103 (= E_5 (_ bv1 32)))).def_114103))
(assert (let ((.def_114106 (= E_4 (_ bv1 32)))).def_114106))
(assert (let ((.def_114109 (= E_3 (_ bv1 32)))).def_114109))
(assert (let ((.def_114112 (= E_2 (_ bv1 32)))).def_114112))
(assert (let ((.def_114115 (= E_M (_ bv1 32)))).def_114115))
(assert (let ((.def_114118 (= E_10 (_ bv1 32)))).def_114118))
(assert (let ((.def_114121 (= E_11 (_ bv1 32)))).def_114121))
(assert (let ((.def_114124 (= t8_st (_ bv0 32)))).def_114124))
(assert (let ((.def_114127 (= t1_st (_ bv0 32)))).def_114127))
(assert (let ((.def_114130 (= t5_st (_ bv0 32)))).def_114130))
(assert (let ((.def_114133 (= t6_st (_ bv0 32)))).def_114133))
(assert (let ((.def_114136 (= t4_st (_ bv0 32)))).def_114136))
(assert (let ((.def_114139 (= t3_st (_ bv0 32)))).def_114139))
(assert (let ((.def_114142 (= t9_st (_ bv0 32)))).def_114142))
(assert (let ((.def_114145 (= t2_st (_ bv0 32)))).def_114145))
(assert (let ((.def_114148 (= t7_st (_ bv0 32)))).def_114148))
(assert (let ((.def_114151 (= t10_st (_ bv0 32)))).def_114151))
(assert (let ((.def_114154 (= t11_st (_ bv0 32)))).def_114154))
(assert (let ((.def_114157 (= E_1 (_ bv1 32)))).def_114157))
(assert (let ((.def_114160 (bvadd (_ bv11 32) local)))(let ((.def_114162 (= .def_114160 token))).def_114162)))

start_simulation2 N1494:
(assert (let ((.def_8237 (= m_pc (_ bv0 32)))).def_8237))
(assert (let ((.def_660 (= c_last_read p_last_write))).def_660))
(assert (let ((.def_669 (= c_num_read p_num_write))).def_669))
(assert (let ((.def_19060 (= t1_pc (_ bv0 32)))).def_19060))
(assert (let ((.def_19063 (= t2_pc (_ bv0 32)))).def_19063))
(assert (let ((.def_19066 (= t3_pc (_ bv0 32)))).def_19066))
(assert (let ((.def_19069 (= t4_pc (_ bv0 32)))).def_19069))
(assert (let ((.def_19072 (= t5_pc (_ bv0 32)))).def_19072))
(assert (let ((.def_19075 (= t6_pc (_ bv0 32)))).def_19075))
(assert (let ((.def_19078 (= t7_pc (_ bv0 32)))).def_19078))
(assert (let ((.def_19081 (= t8_pc (_ bv0 32)))).def_19081))
(assert (let ((.def_19084 (= t9_pc (_ bv0 32)))).def_19084))
(assert (let ((.def_19087 (= t10_pc (_ bv0 32)))).def_19087))
(assert (let ((.def_19090 (= t11_pc (_ bv0 32)))).def_19090))
(assert (let ((.def_19092 (= m_pc (_ bv1 32)))).def_19092))
(assert (let ((.def_19094 (= t1_pc (_ bv1 32)))).def_19094))
(assert (let ((.def_19096 (= t2_pc (_ bv1 32)))).def_19096))
(assert (let ((.def_19098 (= t3_pc (_ bv1 32)))).def_19098))
(assert (let ((.def_19100 (= t4_pc (_ bv1 32)))).def_19100))
(assert (let ((.def_19102 (= t5_pc (_ bv1 32)))).def_19102))
(assert (let ((.def_19104 (= t6_pc (_ bv1 32)))).def_19104))
(assert (let ((.def_19106 (= t7_pc (_ bv1 32)))).def_19106))
(assert (let ((.def_19108 (= t8_pc (_ bv1 32)))).def_19108))
(assert (let ((.def_19110 (= t9_pc (_ bv1 32)))).def_19110))
(assert (let ((.def_19112 (= t10_pc (_ bv1 32)))).def_19112))
(assert (let ((.def_19114 (= t11_pc (_ bv1 32)))).def_19114))
(assert (let ((.def_114091 (= E_9 (_ bv1 32)))).def_114091))
(assert (let ((.def_114094 (= E_8 (_ bv1 32)))).def_114094))
(assert (let ((.def_114097 (= E_7 (_ bv1 32)))).def_114097))
(assert (let ((.def_114100 (= E_6 (_ bv1 32)))).def_114100))
(assert (let ((.def_114103 (= E_5 (_ bv1 32)))).def_114103))
(assert (let ((.def_114106 (= E_4 (_ bv1 32)))).def_114106))
(assert (let ((.def_114109 (= E_3 (_ bv1 32)))).def_114109))
(assert (let ((.def_114112 (= E_2 (_ bv1 32)))).def_114112))
(assert (let ((.def_114115 (= E_M (_ bv1 32)))).def_114115))
(assert (let ((.def_114118 (= E_10 (_ bv1 32)))).def_114118))
(assert (let ((.def_114121 (= E_11 (_ bv1 32)))).def_114121))
(assert (let ((.def_114124 (= t8_st (_ bv0 32)))).def_114124))
(assert (let ((.def_114127 (= t1_st (_ bv0 32)))).def_114127))
(assert (let ((.def_19117 (= m_st (_ bv0 32)))).def_19117))

