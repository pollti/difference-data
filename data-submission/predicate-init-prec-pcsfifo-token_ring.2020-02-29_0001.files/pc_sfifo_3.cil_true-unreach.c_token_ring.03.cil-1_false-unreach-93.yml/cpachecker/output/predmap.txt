(set-info :source |printed by MathSAT|)
(declare-fun c_num_read () (_ BitVec 32))
(declare-fun p_num_write () (_ BitVec 32))
(declare-fun c_last_read () (_ BitVec 32))
(declare-fun p_last_write () (_ BitVec 32))
(declare-fun c_dr_pc () (_ BitVec 32))
(declare-fun c_dr_st () (_ BitVec 32))
(declare-fun m_pc () (_ BitVec 32))
(declare-fun t1_pc () (_ BitVec 32))
(declare-fun t2_pc () (_ BitVec 32))
(declare-fun t3_pc () (_ BitVec 32))
(declare-fun m_st () (_ BitVec 32))

*:
(assert (let ((.def_662 (= c_num_read p_num_write))).def_662))
(assert (let ((.def_671 (= c_last_read p_last_write))).def_671))

error1 N4:
(assert false)
(assert (let ((.def_662 (= c_num_read p_num_write))).def_662))
(assert (let ((.def_671 (= c_last_read p_last_write))).def_671))

eval1 N282:
(assert (let ((.def_822 (= c_dr_pc (_ bv0 32)))).def_822))
(assert (let ((.def_662 (= c_num_read p_num_write))).def_662))
(assert (let ((.def_671 (= c_last_read p_last_write))).def_671))
(assert (let ((.def_1313 (= c_dr_pc (_ bv2 32)))).def_1313))
(assert (let ((.def_1316 (= c_dr_st (_ bv0 32)))).def_1316))

start_simulation1 N338:
(assert (let ((.def_822 (= c_dr_pc (_ bv0 32)))).def_822))
(assert (let ((.def_662 (= c_num_read p_num_write))).def_662))
(assert (let ((.def_671 (= c_last_read p_last_write))).def_671))
(assert (let ((.def_1313 (= c_dr_pc (_ bv2 32)))).def_1313))

error2 N393:
(assert false)
(assert (let ((.def_662 (= c_num_read p_num_write))).def_662))
(assert (let ((.def_671 (= c_last_read p_last_write))).def_671))

eval2 N648:
(assert (let ((.def_4222 (= m_pc (_ bv0 32)))).def_4222))
(assert (let ((.def_662 (= c_num_read p_num_write))).def_662))
(assert (let ((.def_671 (= c_last_read p_last_write))).def_671))
(assert (let ((.def_5854 (= t1_pc (_ bv0 32)))).def_5854))
(assert (let ((.def_5857 (= t2_pc (_ bv0 32)))).def_5857))
(assert (let ((.def_5860 (= t3_pc (_ bv0 32)))).def_5860))
(assert (let ((.def_5862 (= m_pc (_ bv1 32)))).def_5862))
(assert (let ((.def_5864 (= t1_pc (_ bv1 32)))).def_5864))
(assert (let ((.def_5866 (= t2_pc (_ bv1 32)))).def_5866))
(assert (let ((.def_5868 (= t3_pc (_ bv1 32)))).def_5868))
(assert (let ((.def_5871 (= m_st (_ bv0 32)))).def_5871))

start_simulation2 N880:
(assert (let ((.def_4222 (= m_pc (_ bv0 32)))).def_4222))
(assert (let ((.def_662 (= c_num_read p_num_write))).def_662))
(assert (let ((.def_671 (= c_last_read p_last_write))).def_671))
(assert (let ((.def_5854 (= t1_pc (_ bv0 32)))).def_5854))
(assert (let ((.def_5857 (= t2_pc (_ bv0 32)))).def_5857))
(assert (let ((.def_5860 (= t3_pc (_ bv0 32)))).def_5860))
(assert (let ((.def_5862 (= m_pc (_ bv1 32)))).def_5862))
(assert (let ((.def_5864 (= t1_pc (_ bv1 32)))).def_5864))
(assert (let ((.def_5866 (= t2_pc (_ bv1 32)))).def_5866))
(assert (let ((.def_5868 (= t3_pc (_ bv1 32)))).def_5868))

