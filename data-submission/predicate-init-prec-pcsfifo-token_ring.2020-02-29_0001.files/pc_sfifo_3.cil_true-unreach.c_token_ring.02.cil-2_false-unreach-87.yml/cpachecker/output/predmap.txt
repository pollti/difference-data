(set-info :source |printed by MathSAT|)
(declare-fun c_last_read () (_ BitVec 32))
(declare-fun p_last_write () (_ BitVec 32))
(declare-fun c_num_read () (_ BitVec 32))
(declare-fun p_num_write () (_ BitVec 32))
(declare-fun c_dr_pc () (_ BitVec 32))
(declare-fun c_dr_st () (_ BitVec 32))
(declare-fun m_pc () (_ BitVec 32))
(declare-fun t1_pc () (_ BitVec 32))
(declare-fun t2_pc () (_ BitVec 32))
(declare-fun m_st () (_ BitVec 32))

*:
(assert (let ((.def_647 (= c_last_read p_last_write))).def_647))
(assert (let ((.def_656 (= c_num_read p_num_write))).def_656))

error1 N4:
(assert false)
(assert (let ((.def_647 (= c_last_read p_last_write))).def_647))
(assert (let ((.def_656 (= c_num_read p_num_write))).def_656))

eval1 N282:
(assert (let ((.def_807 (= c_dr_pc (_ bv0 32)))).def_807))
(assert (let ((.def_647 (= c_last_read p_last_write))).def_647))
(assert (let ((.def_656 (= c_num_read p_num_write))).def_656))
(assert (let ((.def_1298 (= c_dr_pc (_ bv2 32)))).def_1298))
(assert (let ((.def_1301 (= c_dr_st (_ bv0 32)))).def_1301))

start_simulation1 N338:
(assert (let ((.def_807 (= c_dr_pc (_ bv0 32)))).def_807))
(assert (let ((.def_647 (= c_last_read p_last_write))).def_647))
(assert (let ((.def_656 (= c_num_read p_num_write))).def_656))
(assert (let ((.def_1298 (= c_dr_pc (_ bv2 32)))).def_1298))

error2 N393:
(assert false)
(assert (let ((.def_647 (= c_last_read p_last_write))).def_647))
(assert (let ((.def_656 (= c_num_read p_num_write))).def_656))

eval2 N594:
(assert (let ((.def_4087 (= m_pc (_ bv0 32)))).def_4087))
(assert (let ((.def_647 (= c_last_read p_last_write))).def_647))
(assert (let ((.def_656 (= c_num_read p_num_write))).def_656))
(assert (let ((.def_5138 (= t1_pc (_ bv0 32)))).def_5138))
(assert (let ((.def_5141 (= t2_pc (_ bv0 32)))).def_5141))
(assert (let ((.def_5143 (= m_pc (_ bv1 32)))).def_5143))
(assert (let ((.def_5145 (= t1_pc (_ bv1 32)))).def_5145))
(assert (let ((.def_5147 (= t2_pc (_ bv1 32)))).def_5147))
(assert (let ((.def_5150 (= m_st (_ bv0 32)))).def_5150))

start_simulation2 N785:
(assert (let ((.def_4087 (= m_pc (_ bv0 32)))).def_4087))
(assert (let ((.def_647 (= c_last_read p_last_write))).def_647))
(assert (let ((.def_656 (= c_num_read p_num_write))).def_656))
(assert (let ((.def_5138 (= t1_pc (_ bv0 32)))).def_5138))
(assert (let ((.def_5141 (= t2_pc (_ bv0 32)))).def_5141))
(assert (let ((.def_5143 (= m_pc (_ bv1 32)))).def_5143))
(assert (let ((.def_5145 (= t1_pc (_ bv1 32)))).def_5145))
(assert (let ((.def_5147 (= t2_pc (_ bv1 32)))).def_5147))

