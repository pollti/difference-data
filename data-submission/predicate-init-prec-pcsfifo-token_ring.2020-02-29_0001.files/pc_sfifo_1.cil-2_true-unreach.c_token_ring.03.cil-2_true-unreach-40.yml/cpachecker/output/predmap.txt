(set-info :source |printed by MathSAT|)
(declare-fun c_last_read () (_ BitVec 32))
(declare-fun p_last_write () (_ BitVec 32))
(declare-fun c_num_read () (_ BitVec 32))
(declare-fun p_num_write () (_ BitVec 32))
(declare-fun c_dr_pc () (_ BitVec 32))
(declare-fun q_free () (_ BitVec 32))
(declare-fun q_buf_0 () (_ BitVec 32))
(declare-fun p_dw_pc () (_ BitVec 32))
(declare-fun q_read_ev () (_ BitVec 32))
(declare-fun c_dr_st () (_ BitVec 32))
(declare-fun p_dw_st () (_ BitVec 32))
(declare-fun m_pc () (_ BitVec 32))
(declare-fun t1_pc () (_ BitVec 32))
(declare-fun t2_pc () (_ BitVec 32))
(declare-fun t3_pc () (_ BitVec 32))
(declare-fun m_st () (_ BitVec 32))
(declare-fun E_3 () (_ BitVec 32))
(declare-fun E_M () (_ BitVec 32))
(declare-fun E_2 () (_ BitVec 32))
(declare-fun t3_st () (_ BitVec 32))
(declare-fun t1_st () (_ BitVec 32))
(declare-fun t2_st () (_ BitVec 32))
(declare-fun local () (_ BitVec 32))
(declare-fun token () (_ BitVec 32))
(declare-fun E_1 () (_ BitVec 32))

*:
(assert (let ((.def_540 (= c_last_read p_last_write))).def_540))
(assert (let ((.def_549 (= c_num_read p_num_write))).def_549))

error1 N4:
(assert false)
(assert (let ((.def_540 (= c_last_read p_last_write))).def_540))
(assert (let ((.def_549 (= c_num_read p_num_write))).def_549))

do_write_p N64:
(assert (let ((.def_884 (= c_dr_pc (_ bv0 32)))).def_884))
(assert (let ((.def_540 (= c_last_read p_last_write))).def_540))
(assert (let ((.def_549 (= c_num_read p_num_write))).def_549))
(assert (let ((.def_987 (= q_free (_ bv1 32)))).def_987))
(assert false)
(assert (let ((.def_1330 (= p_last_write q_buf_0))).def_1330))
(assert (let ((.def_990 (= p_dw_pc (_ bv1 32)))).def_990))
(assert (let ((.def_1519 (bvadd (_ bv1 32) c_num_read)))(let ((.def_1520 (= p_num_write .def_1519))).def_1520)))
(assert (let ((.def_2411 (= q_free (_ bv0 32)))).def_2411))
(assert (let ((.def_1522 (= c_dr_pc (_ bv1 32)))).def_1522))
(assert (let ((.def_5212 (= q_read_ev (_ bv1 32)))).def_5212))

do_read_c N100:
(assert (let ((.def_990 (= p_dw_pc (_ bv1 32)))).def_990))
(assert (let ((.def_987 (= q_free (_ bv1 32)))).def_987))
(assert (let ((.def_540 (= c_last_read p_last_write))).def_540))
(assert (let ((.def_549 (= c_num_read p_num_write))).def_549))
(assert (let ((.def_1330 (= p_last_write q_buf_0))).def_1330))
(assert (let ((.def_1519 (bvadd (_ bv1 32) c_num_read)))(let ((.def_1520 (= p_num_write .def_1519))).def_1520)))
(assert (let ((.def_1522 (= c_dr_pc (_ bv1 32)))).def_1522))
(assert false)

eval1 N170:
(assert (let ((.def_884 (= c_dr_pc (_ bv0 32)))).def_884))
(assert (let ((.def_540 (= c_last_read p_last_write))).def_540))
(assert (let ((.def_549 (= c_num_read p_num_write))).def_549))
(assert (let ((.def_987 (= q_free (_ bv1 32)))).def_987))
(assert (let ((.def_1260 (= p_dw_pc (_ bv0 32)))).def_1260))
(assert (let ((.def_1522 (= c_dr_pc (_ bv1 32)))).def_1522))
(assert (let ((.def_3834 (= c_dr_st (_ bv0 32)))).def_3834))
(assert (let ((.def_990 (= p_dw_pc (_ bv1 32)))).def_990))
(assert (let ((.def_5212 (= q_read_ev (_ bv1 32)))).def_5212))
(assert (let ((.def_5215 (= p_dw_st (_ bv0 32)))).def_5215))
(assert (let ((.def_1330 (= p_last_write q_buf_0))).def_1330))
(assert (let ((.def_1519 (bvadd (_ bv1 32) c_num_read)))(let ((.def_1520 (= p_num_write .def_1519))).def_1520)))

start_simulation1 N221:
(assert (let ((.def_884 (= c_dr_pc (_ bv0 32)))).def_884))
(assert (let ((.def_540 (= c_last_read p_last_write))).def_540))
(assert (let ((.def_549 (= c_num_read p_num_write))).def_549))
(assert (let ((.def_987 (= q_free (_ bv1 32)))).def_987))
(assert (let ((.def_1260 (= p_dw_pc (_ bv0 32)))).def_1260))
(assert (let ((.def_5212 (= q_read_ev (_ bv1 32)))).def_5212))

error2 N260:
(assert false)
(assert (let ((.def_540 (= c_last_read p_last_write))).def_540))
(assert (let ((.def_549 (= c_num_read p_num_write))).def_549))

eval2 N502:
(assert (let ((.def_7165 (= m_pc (_ bv0 32)))).def_7165))
(assert (let ((.def_540 (= c_last_read p_last_write))).def_540))
(assert (let ((.def_549 (= c_num_read p_num_write))).def_549))
(assert (let ((.def_8757 (= t1_pc (_ bv0 32)))).def_8757))
(assert (let ((.def_8760 (= t2_pc (_ bv0 32)))).def_8760))
(assert (let ((.def_8763 (= t3_pc (_ bv0 32)))).def_8763))
(assert (let ((.def_8765 (= m_pc (_ bv1 32)))).def_8765))
(assert (let ((.def_8767 (= t1_pc (_ bv1 32)))).def_8767))
(assert (let ((.def_8769 (= t2_pc (_ bv1 32)))).def_8769))
(assert (let ((.def_8771 (= t3_pc (_ bv1 32)))).def_8771))
(assert (let ((.def_8774 (= m_st (_ bv0 32)))).def_8774))
(assert (let ((.def_10836 (= E_3 (_ bv1 32)))).def_10836))
(assert (let ((.def_10839 (= E_M (_ bv1 32)))).def_10839))
(assert (let ((.def_10842 (= E_2 (_ bv1 32)))).def_10842))
(assert (let ((.def_10845 (= t3_st (_ bv0 32)))).def_10845))
(assert (let ((.def_10848 (= t1_st (_ bv0 32)))).def_10848))
(assert (let ((.def_10851 (= t2_st (_ bv0 32)))).def_10851))
(assert (let ((.def_10854 (bvadd (_ bv3 32) local)))(let ((.def_10856 (= .def_10854 token))).def_10856)))
(assert (let ((.def_15599 (= E_2 (_ bv2 32)))).def_15599))
(assert (let ((.def_15602 (= E_1 (_ bv1 32)))).def_15602))
(assert (let ((.def_15604 (= t2_st (_ bv2 32)))).def_15604))
(assert (let ((.def_15606 (= local token))).def_15606))
(assert (let ((.def_15608 (bvadd (_ bv1 32) local)))(let ((.def_15609 (= token .def_15608))).def_15609)))
(assert (let ((.def_15611 (bvadd (_ bv2 32) local)))(let ((.def_15612 (= token .def_15611))).def_15612)))
(assert (let ((.def_19218 (= E_M (_ bv0 32)))).def_19218))
(assert (let ((.def_19220 (= E_3 (_ bv0 32)))).def_19220))
(assert (let ((.def_19222 (= E_2 (_ bv0 32)))).def_19222))
(assert (let ((.def_19224 (= E_1 (_ bv0 32)))).def_19224))

start_simulation2 N734:
(assert (let ((.def_7165 (= m_pc (_ bv0 32)))).def_7165))
(assert (let ((.def_540 (= c_last_read p_last_write))).def_540))
(assert (let ((.def_549 (= c_num_read p_num_write))).def_549))
(assert (let ((.def_8757 (= t1_pc (_ bv0 32)))).def_8757))
(assert (let ((.def_8760 (= t2_pc (_ bv0 32)))).def_8760))
(assert (let ((.def_8763 (= t3_pc (_ bv0 32)))).def_8763))
(assert (let ((.def_8765 (= m_pc (_ bv1 32)))).def_8765))
(assert (let ((.def_8767 (= t1_pc (_ bv1 32)))).def_8767))
(assert (let ((.def_8769 (= t2_pc (_ bv1 32)))).def_8769))
(assert (let ((.def_8771 (= t3_pc (_ bv1 32)))).def_8771))
(assert (let ((.def_10836 (= E_3 (_ bv1 32)))).def_10836))
(assert (let ((.def_10839 (= E_M (_ bv1 32)))).def_10839))
(assert (let ((.def_10842 (= E_2 (_ bv1 32)))).def_10842))
(assert (let ((.def_10845 (= t3_st (_ bv0 32)))).def_10845))
(assert (let ((.def_8774 (= m_st (_ bv0 32)))).def_8774))
(assert (let ((.def_15599 (= E_2 (_ bv2 32)))).def_15599))
(assert (let ((.def_15602 (= E_1 (_ bv1 32)))).def_15602))
(assert (let ((.def_10851 (= t2_st (_ bv0 32)))).def_10851))
(assert (let ((.def_10848 (= t1_st (_ bv0 32)))).def_10848))
(assert (let ((.def_15604 (= t2_st (_ bv2 32)))).def_15604))
(assert (let ((.def_19218 (= E_M (_ bv0 32)))).def_19218))
(assert (let ((.def_19220 (= E_3 (_ bv0 32)))).def_19220))
(assert (let ((.def_19222 (= E_2 (_ bv0 32)))).def_19222))

