(set-info :source |printed by MathSAT|)
(declare-fun c_num_read () (_ BitVec 32))
(declare-fun p_num_write () (_ BitVec 32))
(declare-fun c_last_read () (_ BitVec 32))
(declare-fun p_last_write () (_ BitVec 32))
(declare-fun c_dr_pc () (_ BitVec 32))
(declare-fun q_free () (_ BitVec 32))
(declare-fun q_buf_0 () (_ BitVec 32))
(declare-fun c_dr_st () (_ BitVec 32))
(declare-fun q_req_up () (_ BitVec 32))
(declare-fun q_write_ev () (_ BitVec 32))
(declare-fun q_read_ev () (_ BitVec 32))
(declare-fun p_dw_pc () (_ BitVec 32))
(declare-fun p_dw_st () (_ BitVec 32))
(declare-fun m_pc () (_ BitVec 32))
(declare-fun t1_pc () (_ BitVec 32))
(declare-fun t2_pc () (_ BitVec 32))
(declare-fun t3_pc () (_ BitVec 32))
(declare-fun t4_pc () (_ BitVec 32))
(declare-fun t5_pc () (_ BitVec 32))
(declare-fun t6_pc () (_ BitVec 32))
(declare-fun t7_pc () (_ BitVec 32))
(declare-fun t8_pc () (_ BitVec 32))
(declare-fun t9_pc () (_ BitVec 32))
(declare-fun m_st () (_ BitVec 32))
(declare-fun E_3 () (_ BitVec 32))
(declare-fun E_2 () (_ BitVec 32))
(declare-fun E_7 () (_ BitVec 32))
(declare-fun E_5 () (_ BitVec 32))
(declare-fun E_4 () (_ BitVec 32))
(declare-fun E_M () (_ BitVec 32))
(declare-fun E_8 () (_ BitVec 32))
(declare-fun t6_st () (_ BitVec 32))
(declare-fun E_9 () (_ BitVec 32))
(declare-fun E_6 () (_ BitVec 32))
(declare-fun t2_st () (_ BitVec 32))
(declare-fun t1_st () (_ BitVec 32))
(declare-fun t8_st () (_ BitVec 32))
(declare-fun t3_st () (_ BitVec 32))
(declare-fun t7_st () (_ BitVec 32))
(declare-fun t5_st () (_ BitVec 32))
(declare-fun t4_st () (_ BitVec 32))
(declare-fun t9_st () (_ BitVec 32))
(declare-fun E_1 () (_ BitVec 32))
(declare-fun local () (_ BitVec 32))
(declare-fun token () (_ BitVec 32))

*:
(assert (let ((.def_619 (= c_num_read p_num_write))).def_619))
(assert (let ((.def_628 (= c_last_read p_last_write))).def_628))

error1 N4:
(assert false)
(assert (let ((.def_619 (= c_num_read p_num_write))).def_619))
(assert (let ((.def_628 (= c_last_read p_last_write))).def_628))

do_write_p N78:
(assert (let ((.def_815 (= c_dr_pc (_ bv0 32)))).def_815))
(assert (let ((.def_619 (= c_num_read p_num_write))).def_619))
(assert (let ((.def_628 (= c_last_read p_last_write))).def_628))
(assert (let ((.def_883 (= q_free (_ bv1 32)))).def_883))
(assert false)
(assert (let ((.def_1067 (= p_last_write q_buf_0))).def_1067))
(assert (let ((.def_1207 (= c_dr_pc (_ bv1 32)))).def_1207))
(assert (let ((.def_1209 (= q_free (_ bv0 32)))).def_1209))
(assert (let ((.def_1213 (bvadd (_ bv1 32) c_num_read)))(let ((.def_1214 (= p_num_write .def_1213))).def_1214)))
(assert (let ((.def_1895 (= c_dr_st (_ bv0 32)))).def_1895))
(assert (let ((.def_4295 (= q_req_up (_ bv1 32)))).def_4295))
(assert (let ((.def_5928 (= q_write_ev (_ bv1 32)))).def_5928))
(assert (let ((.def_5930 (= q_write_ev (_ bv0 32)))).def_5930))
(assert (let ((.def_8265 (= q_read_ev (_ bv1 32)))).def_8265))
(assert (let ((.def_8267 (= q_read_ev (_ bv0 32)))).def_8267))

do_read_c N112:
(assert (let ((.def_883 (= q_free (_ bv1 32)))).def_883))
(assert (let ((.def_619 (= c_num_read p_num_write))).def_619))
(assert (let ((.def_628 (= c_last_read p_last_write))).def_628))
(assert (let ((.def_1067 (= p_last_write q_buf_0))).def_1067))
(assert (let ((.def_1213 (bvadd (_ bv1 32) c_num_read)))(let ((.def_1214 (= p_num_write .def_1213))).def_1214)))
(assert false)
(assert (let ((.def_1209 (= q_free (_ bv0 32)))).def_1209))
(assert (let ((.def_5928 (= q_write_ev (_ bv1 32)))).def_5928))
(assert (let ((.def_5930 (= q_write_ev (_ bv0 32)))).def_5930))
(assert (let ((.def_1211 (= p_dw_pc (_ bv1 32)))).def_1211))
(assert (let ((.def_4295 (= q_req_up (_ bv1 32)))).def_4295))
(assert (let ((.def_2369 (= p_dw_st (_ bv0 32)))).def_2369))
(assert (let ((.def_8265 (= q_read_ev (_ bv1 32)))).def_8265))
(assert (let ((.def_8267 (= q_read_ev (_ bv0 32)))).def_8267))

eval1 N228:
(assert (let ((.def_815 (= c_dr_pc (_ bv0 32)))).def_815))
(assert (let ((.def_619 (= c_num_read p_num_write))).def_619))
(assert (let ((.def_628 (= c_last_read p_last_write))).def_628))
(assert (let ((.def_883 (= q_free (_ bv1 32)))).def_883))
(assert (let ((.def_1025 (= p_dw_pc (_ bv0 32)))).def_1025))
(assert (let ((.def_1207 (= c_dr_pc (_ bv1 32)))).def_1207))
(assert (let ((.def_1209 (= q_free (_ bv0 32)))).def_1209))
(assert (let ((.def_1211 (= p_dw_pc (_ bv1 32)))).def_1211))
(assert (let ((.def_2369 (= p_dw_st (_ bv0 32)))).def_2369))
(assert (let ((.def_1067 (= p_last_write q_buf_0))).def_1067))
(assert (let ((.def_1213 (bvadd (_ bv1 32) c_num_read)))(let ((.def_1214 (= p_num_write .def_1213))).def_1214)))
(assert (let ((.def_1895 (= c_dr_st (_ bv0 32)))).def_1895))
(assert (let ((.def_4295 (= q_req_up (_ bv1 32)))).def_4295))
(assert (let ((.def_5928 (= q_write_ev (_ bv1 32)))).def_5928))
(assert (let ((.def_5930 (= q_write_ev (_ bv0 32)))).def_5930))
(assert (let ((.def_8265 (= q_read_ev (_ bv1 32)))).def_8265))
(assert (let ((.def_8267 (= q_read_ev (_ bv0 32)))).def_8267))

start_simulation1 N283:
(assert (let ((.def_815 (= c_dr_pc (_ bv0 32)))).def_815))
(assert (let ((.def_619 (= c_num_read p_num_write))).def_619))
(assert (let ((.def_628 (= c_last_read p_last_write))).def_628))
(assert (let ((.def_883 (= q_free (_ bv1 32)))).def_883))
(assert (let ((.def_1025 (= p_dw_pc (_ bv0 32)))).def_1025))
(assert (let ((.def_1207 (= c_dr_pc (_ bv1 32)))).def_1207))
(assert (let ((.def_1209 (= q_free (_ bv0 32)))).def_1209))
(assert (let ((.def_1211 (= p_dw_pc (_ bv1 32)))).def_1211))
(assert (let ((.def_1895 (= c_dr_st (_ bv0 32)))).def_1895))
(assert (let ((.def_2369 (= p_dw_st (_ bv0 32)))).def_2369))
(assert (let ((.def_1067 (= p_last_write q_buf_0))).def_1067))
(assert (let ((.def_5928 (= q_write_ev (_ bv1 32)))).def_5928))
(assert (let ((.def_5930 (= q_write_ev (_ bv0 32)))).def_5930))
(assert (let ((.def_8265 (= q_read_ev (_ bv1 32)))).def_8265))
(assert (let ((.def_8267 (= q_read_ev (_ bv0 32)))).def_8267))
(assert (let ((.def_1213 (bvadd (_ bv1 32) c_num_read)))(let ((.def_1214 (= p_num_write .def_1213))).def_1214)))

error2 N328:
(assert false)
(assert (let ((.def_619 (= c_num_read p_num_write))).def_619))
(assert (let ((.def_628 (= c_last_read p_last_write))).def_628))

eval2 N894:
(assert (let ((.def_11348 (= m_pc (_ bv0 32)))).def_11348))
(assert (let ((.def_619 (= c_num_read p_num_write))).def_619))
(assert (let ((.def_628 (= c_last_read p_last_write))).def_628))
(assert (let ((.def_19098 (= t1_pc (_ bv0 32)))).def_19098))
(assert (let ((.def_19101 (= t2_pc (_ bv0 32)))).def_19101))
(assert (let ((.def_19104 (= t3_pc (_ bv0 32)))).def_19104))
(assert (let ((.def_19107 (= t4_pc (_ bv0 32)))).def_19107))
(assert (let ((.def_19110 (= t5_pc (_ bv0 32)))).def_19110))
(assert (let ((.def_19113 (= t6_pc (_ bv0 32)))).def_19113))
(assert (let ((.def_19116 (= t7_pc (_ bv0 32)))).def_19116))
(assert (let ((.def_19119 (= t8_pc (_ bv0 32)))).def_19119))
(assert (let ((.def_19122 (= t9_pc (_ bv0 32)))).def_19122))
(assert (let ((.def_19124 (= m_pc (_ bv1 32)))).def_19124))
(assert (let ((.def_19126 (= t1_pc (_ bv1 32)))).def_19126))
(assert (let ((.def_19128 (= t2_pc (_ bv1 32)))).def_19128))
(assert (let ((.def_19130 (= t3_pc (_ bv1 32)))).def_19130))
(assert (let ((.def_19132 (= t4_pc (_ bv1 32)))).def_19132))
(assert (let ((.def_19134 (= t5_pc (_ bv1 32)))).def_19134))
(assert (let ((.def_19136 (= t6_pc (_ bv1 32)))).def_19136))
(assert (let ((.def_19138 (= t7_pc (_ bv1 32)))).def_19138))
(assert (let ((.def_19140 (= t8_pc (_ bv1 32)))).def_19140))
(assert (let ((.def_19142 (= t9_pc (_ bv1 32)))).def_19142))
(assert (let ((.def_19145 (= m_st (_ bv0 32)))).def_19145))
(assert (let ((.def_48166 (= E_3 (_ bv2 32)))).def_48166))
(assert (let ((.def_48169 (= E_2 (_ bv1 32)))).def_48169))
(assert (let ((.def_48172 (= E_7 (_ bv1 32)))).def_48172))
(assert (let ((.def_48175 (= E_5 (_ bv1 32)))).def_48175))
(assert (let ((.def_48178 (= E_4 (_ bv1 32)))).def_48178))
(assert (let ((.def_48180 (= E_3 (_ bv1 32)))).def_48180))
(assert (let ((.def_48183 (= E_M (_ bv1 32)))).def_48183))
(assert (let ((.def_48186 (= E_8 (_ bv1 32)))).def_48186))
(assert (let ((.def_48189 (= t6_st (_ bv0 32)))).def_48189))
(assert (let ((.def_48192 (= E_9 (_ bv1 32)))).def_48192))
(assert (let ((.def_48195 (= E_6 (_ bv1 32)))).def_48195))
(assert (let ((.def_48198 (= t2_st (_ bv0 32)))).def_48198))
(assert (let ((.def_48201 (= t1_st (_ bv0 32)))).def_48201))
(assert (let ((.def_48204 (= t8_st (_ bv0 32)))).def_48204))
(assert (let ((.def_48207 (= t3_st (_ bv0 32)))).def_48207))
(assert (let ((.def_48210 (= t7_st (_ bv0 32)))).def_48210))
(assert (let ((.def_48213 (= t5_st (_ bv0 32)))).def_48213))
(assert (let ((.def_48216 (= t4_st (_ bv0 32)))).def_48216))
(assert (let ((.def_48219 (= t9_st (_ bv0 32)))).def_48219))
(assert (let ((.def_48222 (= E_1 (_ bv1 32)))).def_48222))
(assert (let ((.def_48225 (bvadd (_ bv9 32) local)))(let ((.def_48227 (= .def_48225 token))).def_48227)))

start_simulation2 N1372:
(assert (let ((.def_11348 (= m_pc (_ bv0 32)))).def_11348))
(assert (let ((.def_619 (= c_num_read p_num_write))).def_619))
(assert (let ((.def_628 (= c_last_read p_last_write))).def_628))
(assert (let ((.def_19098 (= t1_pc (_ bv0 32)))).def_19098))
(assert (let ((.def_19101 (= t2_pc (_ bv0 32)))).def_19101))
(assert (let ((.def_19104 (= t3_pc (_ bv0 32)))).def_19104))
(assert (let ((.def_19107 (= t4_pc (_ bv0 32)))).def_19107))
(assert (let ((.def_19110 (= t5_pc (_ bv0 32)))).def_19110))
(assert (let ((.def_19113 (= t6_pc (_ bv0 32)))).def_19113))
(assert (let ((.def_19116 (= t7_pc (_ bv0 32)))).def_19116))
(assert (let ((.def_19119 (= t8_pc (_ bv0 32)))).def_19119))
(assert (let ((.def_19122 (= t9_pc (_ bv0 32)))).def_19122))
(assert (let ((.def_19124 (= m_pc (_ bv1 32)))).def_19124))
(assert (let ((.def_19126 (= t1_pc (_ bv1 32)))).def_19126))
(assert (let ((.def_19128 (= t2_pc (_ bv1 32)))).def_19128))
(assert (let ((.def_19130 (= t3_pc (_ bv1 32)))).def_19130))
(assert (let ((.def_19132 (= t4_pc (_ bv1 32)))).def_19132))
(assert (let ((.def_19134 (= t5_pc (_ bv1 32)))).def_19134))
(assert (let ((.def_19136 (= t6_pc (_ bv1 32)))).def_19136))
(assert (let ((.def_19138 (= t7_pc (_ bv1 32)))).def_19138))
(assert (let ((.def_19140 (= t8_pc (_ bv1 32)))).def_19140))
(assert (let ((.def_19142 (= t9_pc (_ bv1 32)))).def_19142))
(assert (let ((.def_48166 (= E_3 (_ bv2 32)))).def_48166))
(assert (let ((.def_48169 (= E_2 (_ bv1 32)))).def_48169))
(assert (let ((.def_48172 (= E_7 (_ bv1 32)))).def_48172))
(assert (let ((.def_48175 (= E_5 (_ bv1 32)))).def_48175))
(assert (let ((.def_48178 (= E_4 (_ bv1 32)))).def_48178))
(assert (let ((.def_48180 (= E_3 (_ bv1 32)))).def_48180))
(assert (let ((.def_48183 (= E_M (_ bv1 32)))).def_48183))
(assert (let ((.def_48186 (= E_8 (_ bv1 32)))).def_48186))
(assert (let ((.def_48189 (= t6_st (_ bv0 32)))).def_48189))
(assert (let ((.def_48192 (= E_9 (_ bv1 32)))).def_48192))
(assert (let ((.def_48195 (= E_6 (_ bv1 32)))).def_48195))
(assert (let ((.def_48198 (= t2_st (_ bv0 32)))).def_48198))
(assert (let ((.def_48201 (= t1_st (_ bv0 32)))).def_48201))
(assert (let ((.def_19145 (= m_st (_ bv0 32)))).def_19145))

