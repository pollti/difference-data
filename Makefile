# This file is part of the replication artifact for
# difference verification with conditions:
# https://gitlab.com/sosy-lab/research/data/difference-data
#
# SPDX-FileCopyrightText: 2020 Dirk Beyer <https://sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

RESULTS_DIR ?= data_raw/
SRESULTS_DIR ?= singres/
CPACHECKER_DIR ?= cpachecker
CPASEQ_DIR ?= cpa-seq
UAUTOMIZER_DIR ?= uautomizer

PRIORITY := "LOW"
CLIENT_HEAP := 6000
CPU := "1230"
CLOUD_OPTIONS=--no-container --cloud --revision diffCond-prop:39252 --cloudMaster https://vcloud.sosy-lab.org/cpachecker/webclient/ @/home/sysop/.vcloud-creds --cloudCPUModel $(CPU) --cloudClientHeap $(CLIENT_HEAP) # --debug # --cloudPriority $(PRIORITY)
BENCHMARK_OPTIONS := -o $(RESULTS_DIR)
# Uncomment this line to run benchmarks on SoSy-Lab's cluster (only available for our own chair)
BENCHMARK_OPTIONS := $(CLOUD_OPTIONS) -o $(RESULTS_DIR)

TIME ?= "2020-02-29 00:01:00"
TIME_ENC ?= 2020-02-29_00-01-00

PREDICATE_COMBOS := predicate-combos
SINGLE := bpredicate-single

RUNDEF_FULL ?= fullAnalysis
RUNDEF_DIFF_RUN ?= diffAnalysis-ignoreDecl
RUNDEF_DIFFPRO_DECL_RUN ?= diffPropAnalysisProp
RUNDEF_DIFFPRO_RUN ?= diffPropAnalysis-ignoreDecl
RUNDEF_DIFFPRO_NOIMPLY_RUN ?= diffPropAnalysis-noimply-ignoreDecl
RUNDEF_DIFFPRO_NOMERGE_RUN ?= diffPropAnalysis-nomerge-ignoreDecl
RUNDEF_DIFFPRO_NOMERGEIMPL_RUN ?= diffPropAnalysis-nomerge-noimply-ignoreDecl
RUNDEF_DIFFPRO_NOMERGEPREP_RUN ?= diffPropAnalysis-nomerge-noprep-ignoreDecl
RUNDEF_DIFFPRO_NOPREP_RUN ?= diffPropAnalysis-nopreprocessing-ignoreDecl

BENCHMARK_SET ?= eca-token_ring gcd-newton pals-eca pcsfifo-token_ring square_softfloat # ssh-simplified
SINGLE_SET ?= ec2 eca flt gcd new pal sfi squ tok
BENCHMARK_DEF_DIR := benchmark_defs

all: tasks diffpro-predicate diffpro-noimply-predicate diffpro-nomerge-predicate diffpro-noprep-predicate fully diff-predicate diffprop-nomerge-noimply-predicate diffpro-nomerge-noprep-predicate

DATE := $(shell date +%Y-%m-%d)
clean:
	mkdir -p data_backups/$(DATE)
	mv $(RESULTS_DIR)* data_backups/$(DATE) | true
	rm -rf cpa-seq uautomizer # just to clean up parts from other analysis calls on different makefile versions

tools: cpachecker/cpachecker.jar

cpachecker/cpachecker.jar:
	git submodule update --init cpachecker
	cd cpachecker && \
		ant && ant clean build && ant jar

$(RESULTS_DIR):
	mkdir -p $(RESULTS_DIR)

initial-selection: $(RESULTS_DIR)predicate-initial-selection.$(TIME_ENC).results.fullAnalysis.xml.bz2

fully: $(foreach set, $(BENCHMARK_SET), $(RESULTS_DIR)$(PREDICATE_COMBOS)-$(set).$(RUNDEF_FULL).$(TIME_ENC).results.$(RUNDEF_FULL).xml.bz2)

diff-predicate: $(foreach set, $(BENCHMARK_SET), $(RESULTS_DIR)$(PREDICATE_COMBOS)-$(set).$(RUNDEF_DIFF_RUN).$(TIME_ENC).results.$(RUNDEF_DIFF_RUN).xml.bz2)

diffpro-predicate: $(foreach set, $(BENCHMARK_SET), $(RESULTS_DIR)$(PREDICATE_COMBOS)-$(set).$(RUNDEF_DIFFPRO_RUN).$(TIME_ENC).results.$(RUNDEF_DIFFPRO_RUN).xml.bz2)

diffpro-noimply-predicate: $(foreach set, $(BENCHMARK_SET), $(RESULTS_DIR)$(PREDICATE_COMBOS)-$(set).$(RUNDEF_DIFFPRO_NOIMPLY_RUN).$(TIME_ENC).results.$(RUNDEF_DIFFPRO_NOIMPLY_RUN).xml.bz2)

diffpro-nomerge-predicate: $(foreach set, $(BENCHMARK_SET), $(RESULTS_DIR)$(PREDICATE_COMBOS)-$(set).$(RUNDEF_DIFFPRO_NOMERGE_RUN).$(TIME_ENC).results.$(RUNDEF_DIFFPRO_NOMERGE_RUN).xml.bz2)

diffpro-nomerge-noimply-predicate: $(foreach set, $(BENCHMARK_SET), $(RESULTS_DIR)$(PREDICATE_COMBOS)-$(set).$(RUNDEF_DIFFPRO_NOMERGEIMPL_RUN).$(TIME_ENC).results.$(RUNDEF_DIFFPRO_NOMERGEIMPL_RUN).xml.bz2)
diffpro-nomerge-noprep-predicate: $(foreach set, $(BENCHMARK_SET), $(RESULTS_DIR)$(PREDICATE_COMBOS)-$(set).$(RUNDEF_DIFFPRO_NOMERGEPREP_RUN).$(TIME_ENC).results.$(RUNDEF_DIFFPRO_NOMERGEPREP_RUN).xml.bz2)

diffpro-noprep-predicate: $(foreach set, $(BENCHMARK_SET), $(RESULTS_DIR)$(PREDICATE_COMBOS)-$(set).$(RUNDEF_DIFFPRO_NOPREP_RUN).$(TIME_ENC).results.$(RUNDEF_DIFFPRO_NOPREP_RUN).xml.bz2)



single-dp: $(foreach set, $(SINGLE_SET), $(SRESULTS_DIR)$(SINGLE)-$(set).$(RUNDEF_DIFFPRO_RUN).$(TIME_ENC).results.$(RUNDEF_DIFFPRO_RUN).xml.bz2)

single-d: $(foreach set, $(SINGLE_SET), $(SRESULTS_DIR)$(SINGLE)-$(set).$(RUNDEF_DIFF_RUN).$(TIME_ENC).results.$(RUNDEF_DIFF_RUN).xml.bz2)

single-full: $(foreach set, $(SINGLE_SET), $(SRESULTS_DIR)$(SINGLE)-$(set).$(RUNDEF_FULL).$(TIME_ENC).results.$(RUNDEF_FULL).xml.bz2)







include Makefile.tasks

$(RESULTS_DIR)predicate-initial-selection.$(TIME_ENC).results.fullAnalysis.xml.bz2: benchmark_defs/predicate-initial-selection.xml $(RESULTS_DIR)
	$(CPACHECKER_DIR)/scripts/benchmark.py $(BENCHMARK_OPTIONS) --startTime $(TIME) $<

$(RESULTS_DIR)$(PREDICATE_COMBOS)-%.$(RUNDEF_FULL).$(TIME_ENC).results.$(RUNDEF_FULL).xml.bz2: benchmark_defs/$(PREDICATE_COMBOS)-%.xml $(RESULTS_DIR)
	$(CPACHECKER_DIR)/scripts/benchmark.py $(BENCHMARK_OPTIONS) -r $(RUNDEF_FULL) -n $(RUNDEF_FULL) --startTime $(TIME) $<

$(RESULTS_DIR)$(PREDICATE_COMBOS)-%.$(RUNDEF_DIFF_RUN).$(TIME_ENC).results.$(RUNDEF_DIFF_RUN).xml.bz2: $(BENCHMARK_DEF_DIR)/$(PREDICATE_COMBOS)-%.xml $(RESULTS_DIR)
	$(CPACHECKER_DIR)/scripts/benchmark.py $(BENCHMARK_OPTIONS) -r $(RUNDEF_DIFF_RUN) -n $(RUNDEF_DIFF_RUN) --startTime $(TIME) $<

$(RESULTS_DIR)$(PREDICATE_COMBOS)-%.$(RUNDEF_DIFFPRO_DECL_RUN).$(TIME_ENC).results.$(RUNDEF_DIFFPRO_DECL_RUN).xml.bz2: $(BENCHMARK_DEF_DIR)/$(PREDICATE_COMBOS)-%.xml $(RESULTS_DIR)
	$(CPACHECKER_DIR)/scripts/benchmark.py $(RUNDEF_DIFFPRO_DECL_RUN) -r $(RUNDEF_DIFFPRO_DECL_RUN) -n $(RUNDEF_DIFFPRO_DECL_RUN) --startTime $(TIME) $<

$(RESULTS_DIR)$(PREDICATE_COMBOS)-%.$(RUNDEF_DIFFPRO_RUN).$(TIME_ENC).results.$(RUNDEF_DIFFPRO_RUN).xml.bz2: $(BENCHMARK_DEF_DIR)/$(PREDICATE_COMBOS)-%.xml $(RESULTS_DIR)
	$(CPACHECKER_DIR)/scripts/benchmark.py $(BENCHMARK_OPTIONS) -r $(RUNDEF_DIFFPRO_RUN) -n $(RUNDEF_DIFFPRO_RUN) --startTime $(TIME) $<

$(RESULTS_DIR)$(PREDICATE_COMBOS)-%.$(RUNDEF_DIFFPRO_NOPREP_RUN).$(TIME_ENC).results.$(RUNDEF_DIFFPRO_NOPREP_RUN).xml.bz2: $(BENCHMARK_DEF_DIR)/$(PREDICATE_COMBOS)-%.xml $(RESULTS_DIR)
	$(CPACHECKER_DIR)/scripts/benchmark.py $(BENCHMARK_OPTIONS) -r $(RUNDEF_DIFFPRO_NOPREP_RUN) -n $(RUNDEF_DIFFPRO_NOPREP_RUN) --startTime $(TIME) $<
	
$(RESULTS_DIR)$(PREDICATE_COMBOS)-%.$(RUNDEF_DIFFPRO_NOIMPLY_RUN).$(TIME_ENC).results.$(RUNDEF_DIFFPRO_NOIMPLY_RUN).xml.bz2: $(BENCHMARK_DEF_DIR)/$(PREDICATE_COMBOS)-%.xml $(RESULTS_DIR)
	$(CPACHECKER_DIR)/scripts/benchmark.py $(BENCHMARK_OPTIONS) -r $(RUNDEF_DIFFPRO_NOIMPLY_RUN) -n $(RUNDEF_DIFFPRO_NOIMPLY_RUN) --startTime $(TIME) $<

$(RESULTS_DIR)$(PREDICATE_COMBOS)-%.$(RUNDEF_DIFFPRO_NOMERGE_RUN).$(TIME_ENC).results.$(RUNDEF_DIFFPRO_NOMERGE_RUN).xml.bz2: $(BENCHMARK_DEF_DIR)/$(PREDICATE_COMBOS)-%.xml $(RESULTS_DIR)
	$(CPACHECKER_DIR)/scripts/benchmark.py $(BENCHMARK_OPTIONS) -r $(RUNDEF_DIFFPRO_NOMERGE_RUN) -n $(RUNDEF_DIFFPRO_NOMERGE_RUN) --startTime $(TIME) $<

$(RESULTS_DIR)$(PREDICATE_COMBOS)-%.$(RUNDEF_DIFFPRO_NOMERGEPREP_RUN).$(TIME_ENC).results.$(RUNDEF_DIFFPRO_NOMERGEPREP_RUN).xml.bz2: $(BENCHMARK_DEF_DIR)/$(PREDICATE_COMBOS)-%.xml $(RESULTS_DIR)
	$(CPACHECKER_DIR)/scripts/benchmark.py $(BENCHMARK_OPTIONS) -r $(RUNDEF_DIFFPRO_NOMERGEPREP_RUN) -n $(RUNDEF_DIFFPRO_NOMERGEPREP_RUN) --startTime $(TIME) $<

$(RESULTS_DIR)$(PREDICATE_COMBOS)-%.$(RUNDEF_DIFFPRO_NOMERGEIMPL_RUN).$(TIME_ENC).results.$(RUNDEF_DIFFPRO_NOMERGEIMPL_RUN).xml.bz2: $(BENCHMARK_DEF_DIR)/$(PREDICATE_COMBOS)-%.xml $(RESULTS_DIR)
	$(CPACHECKER_DIR)/scripts/benchmark.py $(BENCHMARK_OPTIONS) -r $(RUNDEF_DIFFPRO_NOMERGEIMPL_RUN) -n $(RUNDEF_DIFFPRO_NOMERGEIMPL_RUN) --startTime $(TIME) $<

$(RESULTS_DIR)$(PREDICATE_COMBOS)-%.$(RUNDEF_DIFFPRO_NOPREP_RUN).$(TIME_ENC).results.$(RUNDEF_DIFFPRO_NOPREP_RUN).xml.bz2: $(BENCHMARK_DEF_DIR)/$(PREDICATE_COMBOS)-%.xml $(RESULTS_DIR)
	$(CPACHECKER_DIR)/scripts/benchmark.py $(BENCHMARK_OPTIONS) -r $(RUNDEF_DIFFPRO_NOPREP_RUN) -n $(RUNDEF_DIFFPRO_NOPREP_RUN) --startTime $(TIME) $<







$(SRESULTS_DIR)$(SINGLE)-%.$(RUNDEF_DIFF_RUN).$(TIME_ENC).results.$(RUNDEF_DIFF_RUN).xml.bz2: $(BENCHMARK_DEF_DIR)/$(SINGLE)-%.xml $(SRESULTS_DIR)
	$(CPACHECKER_DIR)/scripts/benchmark.py $(BENCHMARK_OPTIONS) -r $(RUNDEF_DIFF_RUN) -n $(RUNDEF_DIFF_RUN) --startTime $(TIME) $<

$(SRESULTS_DIR)$(SINGLE)-%.$(RUNDEF_DIFFPRO_RUN).$(TIME_ENC).results.$(RUNDEF_DIFFPRO_RUN).xml.bz2: $(BENCHMARK_DEF_DIR)/$(SINGLE)-%.xml $(SRESULTS_DIR)
	$(CPACHECKER_DIR)/scripts/benchmark.py $(BENCHMARK_OPTIONS) -r $(RUNDEF_DIFFPRO_RUN) -n $(RUNDEF_DIFFPRO_RUN) --startTime $(TIME) $<

$(SRESULTS_DIR)$(SINGLE)-%.$(RUNDEF_FULL).$(TIME_ENC).results.$(RUNDEF_FULL).xml.bz2: $(BENCHMARK_DEF_DIR)/$(SINGLE)-%.xml $(SRESULTS_DIR)
	$(CPACHECKER_DIR)/scripts/benchmark.py $(BENCHMARK_OPTIONS) -r $(RUNDEF_FULL) -n $(RUNDEF_FULL) --startTime $(TIME) $<
